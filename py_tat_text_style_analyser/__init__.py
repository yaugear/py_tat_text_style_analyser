# -*- coding: UTF-8 -*-

import os
import random

from sklearn import svm
from sklearn.externals import joblib
from py_tat_morphan.morphan import Morphan

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
ATTRIBUTES_FILE = os.path.join(BASE_DIR, "files/attributes.tsv")
MODEL_FILE = os.path.join(BASE_DIR, "files/svm_text_styler.pkl")
STYLES = [u"Художественный", u"Публицистический", u"Официально-деловой", u"Научный"]
SHORT = ['sentences_len', 'n_chain_len', 'v_chain_len', 
         'unique_wordforms', 'unique_lemmas', 
         'N', 'V', 'POST', 'PART', 'INTRJ', 
         'Num', 'MOD', 'Adj', 'CNJ', 'IMIT', 
         'PN', 'Adv', 'PROP', 'NR', 'Type2',
         'Type3', 'Type4', 'Rus', 'Letter', 'Latin'
        ]

def load_attributes(attribute_file=None):
    """
        loads attribute from .tsv file

        :attribute_file filename
    """
    if not attribute_file:
        attribute_file = ATTRIBUTES_FILE

    with open(attribute_file, 'r') as stream:
        data = stream.read().decode('utf-8')

    attributes = []
    for line in data.split('\n'):
        attribute_type, title, min_value, max_value = line.split('\t')
        attributes.append({"type": int(attribute_type), 
                           "title": title, 
                           "min": float(min_value), 
                           "max": float(max_value)})
    return attributes


def get_text_attributes(text, attributes):
    """
        Analyses text and get count of each attribute

        :text plain text in Tatar language
        :attributes list of dictionary
    """
    # loads Tatar Morphological Analyser
    morphan = Morphan()
    sentences = morphan.analyse_text(text)

    attributes = [attribute.copy() for attribute in attributes]
    for attribute in attributes:
        attribute["value"] = 0

    words_count = 0
    sentences_count = len(sentences)
    
    # 3. - коэффициент  для 1000 слов - количество уникальных словоформ
    wordforms = []
    wordforms_count_1000 = 0
    wordforms_portion = 0
    # 2. - коэффициент для 1000 слов - количество лемм
    lemmas = []
    lemmas_count_1000 = 0
    lemmas_portion = 0
    # 4 - длина аффиксальной цепочки у сущ.
    n_chain_len = 0
    n_count = 0
    # 5 - длина аффиксальной цепочки у глаголов
    v_chain_len = 0
    v_count = 0
    
    for sentence in sentences:
        words_count += len(sentence)
        for word in sentence:
            for attribute in attributes:
                if attribute["type"] in [0, 1] and attribute["title"] in word[attribute["type"]]:
                    attribute["value"] += 1
                    
                # 3. - коэффициент  для 1000 слов - количество уникальных словоформ
                if len(wordforms) < 1000:
                    wordforms.append(word[0])
                else:
                    wordforms_count_1000 += len(set(wordforms))
                    wordforms = []
                    wordforms_portion += 1
                    
                # 2. - коэффициент  для 1000 слов (или для любой другой выборки) - количество лемм
                if len(lemmas) < 1000:
                    lemmas.append(word[1].split('+')[0])
                else:
                    lemmas_count_1000 += len(set(lemmas))
                    lemmas = []
                    lemmas_portion += 1
                for morph in word[1].split(';'):
                    if len(morph.split('+')) > 1:
                        if morph.split('+')[1] == 'N':
                            # 4 - длина аффиксальной цепочки у сущ.
                            n_chain_len += len(morph.split('+')) - 1
                            n_count += 1
                        elif morph.split('+')[1] == 'V':
                            # 5 - длина аффиксальной цепочки у глаголов
                            v_chain_len += len(morph.split('+')) - 1
                            v_count += 1
    if n_chain_len > 0:
        n_chain_len = float(n_chain_len) / n_count
    if v_chain_len > 0:
        v_chain_len = float(v_chain_len) / v_count
    if wordforms_portion > 0:
        wordforms_count_1000 = wordforms_count_1000 / wordforms_portion
    if lemmas_portion > 0:
        lemmas_portion = lemmas_count_1000 / lemmas_portion
    sentences_len = words_count / sentences_count

    for attribute in attributes:
        if attribute["title"] == "n_chain_len":
            attribute["value"] = n_chain_len
        elif attribute["title"] == "v_chain_len":
            attribute["value"] = v_chain_len
        elif attribute["title"] == "unique_wordforms":
            attribute["value"] = wordforms_count_1000
        elif attribute["title"] == "unique_lemmas":
            attribute["value"] = lemmas_count_1000
        elif attribute["title"] == "sentences_len":
            attribute["value"] = sentences_len
        else:
            attribute["value"] = float(attribute["value"]) * 100 / words_count
    return attributes


def get_trained_model(metafile, attributes=None):
    """
        Trains model on given data
    """
    if not attributes:
        attributes = SHORT

    with open(metafile, 'r') as stream:
        meta = stream.read().decode('utf-8').split('\n')

    all_attributes = load_attributes()

    data = []
    # every file is converted into vector accroding to used attributes
    for line in meta:
        filename, style = line.split('\t')
        with open(filename, 'r') as stream:
            text = stream.read().decode('utf-8')
        row = redesign_data(get_text_attributes(text, all_attributes), attributes)
        if style in STYLES:
            data.append((row, STYLES.index(style)))

    random.shuffle(data)
    train_data = []
    targets = []
    for row, target in data:
        train_data.append(row)
        targets.append(target)

    # init and fits the model
    text_style_analyser = svm.SVC(gamma=0.001, C=100., probability=True)
    text_style_analyser.fit(train_data, targets)

    return text_style_analyser


def redesign_data(data, attributes):
    """
        Orders and redesings data to fit trained model
        :data list of dictionaries
        :attributes list of attribute titles
    """
    result = []
    tmp_attributes = {}
    for attribute in data:
        tmp_attributes[attribute["title"]] = float(attribute["value"] - attribute["min"]) \
                                             / (attribute["max"] - attribute["min"])

    for attribute in attributes:
        result.append(tmp_attributes[attribute])
    return result


def define_style(filename, svm_text_styler=None, attributes=None):
    """
        Main function of style definer
    """
    # loads pretrained model
    if not svm_text_styler:
        svm_text_styler = joblib.load(MODEL_FILE)
    if not attributes:
        attributes = SHORT

    # loads attributes with min and max values
    all_attributes = load_attributes()
    # load text
    with open(filename, 'r') as stream:
        text = stream.read().decode('utf-8')
    # and gets all attributes values for this text
    data = get_text_attributes(text, all_attributes)

    # redesigns data to fit the model data format
    # additionally normalyses it into [0.0, 1.0]
    result = svm_text_styler.predict([redesign_data(data, attributes)])
    if len(result) == 1:
        return STYLES[result[0]]
    else:
        return 'NR'


def define_style_prob(filename, svm_text_styler=None, attributes=None):
    """
        Main function of style definer with prob
    """
    # loads pretrained model
    if not svm_text_styler:
        svm_text_styler = joblib.load(MODEL_FILE)
    if not attributes:
        attributes = SHORT

    # loads attributes with min and max values
    all_attributes = load_attributes()
    # load text
    with open(filename, 'r') as stream:
        text = stream.read().decode('utf-8')
    # and gets all attributes values for this text
    data = get_text_attributes(text, all_attributes)

    # redesigns data to fit the model data format
    # additionally normalyses it into [0.0, 1.0]
    result = svm_text_styler.predict_proba([redesign_data(data, attributes)])
    return dict(zip(STYLES, result[0]))

def save_model(svm_text_styler_model, filename):
    joblib.dump(svm_text_styler_model, filename)

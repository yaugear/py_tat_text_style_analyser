from setuptools import setup

setup(name='tat-text-style-analyser',
      version='0.1',
      description='Machine Translator from Tatar To Russina languages (and vice-versa)',
      url='https://bitbucket.org/yaugear/py_tat_text_style_analyser',
      author='Yaugear',
      author_email='ramil.gata@gmail.com',
      keywords=['nlp', 'Tatar language'],
      license='MIT',
      packages=['py_tat_text_style_analyser'],
      scripts=['bin/define_text_style'],
      install_requires=['py_tat_morphan',
                        'sklearn'],
      include_package_data=True,
      test_suite='nose.collector',
      tests_require=['nose'],
      setup_requires=['nose>=1.0'],
      zip_safe=False
     )

Tatar Text Style Analyser
=========================

To install:
-----------

	$ git clone https://yaugear@bitbucket.org/yaugear/py_tat_text_style_analyser.git

	$ cd py_tat_text_style_analyser

	$ python setup.py install

	$ python setup.py test


To use:
-------
    As python package:

    >>> from py_tat_text_style_analyser import define_style, define_style_prob
    >>> define_style('myfile.txt')		// simple result
    >>> define_style_prob('myfile.txt')		// result with probabilities
    

    Or in console:
    $ define_text_style <path/to/textfile>


For feedback:
-------------

	ramil.gata@gmail.com - Ramil Gataullin